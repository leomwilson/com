FROM nginx:stable-alpine

COPY public /var/www/leomwilson.com
COPY nginx.conf /etc/nginx/nginx.conf
